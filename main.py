from yad2_api import get_yad2_update, get_total_tlv_pages, get_yad2_page, filter_irrelevant
import json
from datetime import datetime
import os

def print_house_data(data):
    for h in data:
        try:
            print(f"{h['neighborhood']=}")
        except KeyError:
            pass
        print(f"{h['street']=}")
        print(f"{h['price']=}")
        print(f"{h['estateType']=}")
        print(f"{h['rooms']=}")
        print(f"{h['updatedAt']=}")
        print(f"{h['linkToken']=}\n")
        #print(f"KeyError {e} {h}\n")

def combine_json_files(folder_name):
    # Get a list of all JSON files in the specified folder
    json_files = [f for f in os.listdir(folder_name) if f.endswith('.json')]

    # Initialize an empty list to store combined data
    combined_data = []

    # Read data from each JSON file and append it to the combined_data list
    for file_name in json_files:
        with open(os.path.join(folder_name, file_name), 'r') as infile:
            data = json.load(infile)
            combined_data.extend(data)

    # Create a new JSON file with the combined data

    time_now = datetime.now().strftime('%Y-%m-%d_%H-%M')

    # Create the file name
    output_file_name = f"final_output/{time_now}.json"

    # output_file_name = f"{datetime.now().strftime('%Y-%m-%d')}.json"
    with open(output_file_name, 'w') as outfile:
        json.dump(combined_data, outfile, ensure_ascii=False)

    # # Delete the original JSON files (except for the output file)
    # for file_name in json_files:
    #     if file_name != output_file_name:
    #         os.remove(os.path.join(folder_name, file_name))

    return output_file_name

def main():
    # with open("example.json","r") as f:
    #     data = json.load(f)

    # data, requst_hash = get_yad2_update()
    # data = filter_irrelevant(data)
    # print_house_data(data)
    # print(f"{requst_hash=}")

    page_num = get_total_tlv_pages()
    print(f"{page_num=}")


    for i in range(page_num+1):
        data, requst_hash = get_yad2_page(i)
        data = filter_irrelevant(data)
        with open(f"cached_pages/page_{i}.json", "w") as f:
            json.dump(data,f, ensure_ascii=False)
        print_house_data(data)
        print(f"dumped to cached_pages/page_{i}")

    output_file = combine_json_files("cached_pages")
    print(f"Merged data written to '{output_file}'")


if __name__ == "__main__":
    main()