import hashlib
import requests


def scraper_get(url: str):
    headers={"User-Agent": "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148"}
    return requests.get(url, headers=headers)

def deseralize_data(response):
    if response.status_code == 200:
        # Convert the JSON response to a dictionary
        data_dict = response.json()

        # print(response.content)
        response_hash = hashlib.md5(response.content).hexdigest()
        # Now you can access the data as a dictionary
        return (data_dict,response_hash)
    else:
        print(f"Error: {response.status_code}")
        return None

def filter_irrelevant(listings):
    listings = listings['items']
    # print(listings)
    ret = []
    for item in listings:
        
        try:
            if item['rooms'] >= 4:
                item['linkToken'] = f"https://www.yad2.co.il/realestate/item/{item['linkToken']}"
                ret.append(item)
        except TypeError:
            print(f"Feild error in item skipping\n{item=}")
            continue
    return ret

def get_yad2_update():
    url = "https://real-estate-smart-agent-server.onrender.com/real-estate/update?searchId=66590f0156b302d78e0c21a7"
    response = scraper_get(url)
    return deseralize_data(response)

def get_total_tlv_pages():
    # url = "https://real-estate-smart-agent-server.onrender.com/real-estate/update?searchId=66590f0156b302d78e0c21a7"
    url = "https://real-estate-smart-agent-server.onrender.com/real-estate/search?dealType=rent&settlement=%D7%AA%D7%9C+%D7%90%D7%91%D7%99%D7%91+-+%D7%99%D7%A4%D7%95&minPrice=9000&maxPrice=13000&page=1&orderId=1&saveToDb=true"
    
    response = scraper_get(url)
    response = deseralize_data(response)
    return response[0]['total_pages']

def get_yad2_page(num):
    url = f"https://real-estate-smart-agent-server.onrender.com/real-estate/search?dealType=rent&settlement=%D7%AA%D7%9C+%D7%90%D7%91%D7%99%D7%91+-+%D7%99%D7%A4%D7%95&minPrice=9000&maxPrice=13000&page={num}&orderId=1&saveToDb=true"
    response = scraper_get(url)
    return deseralize_data(response)

if __name__ == "__main__":
    page_num = get_total_tlv_pages()
    print(f"{page_num=}")



